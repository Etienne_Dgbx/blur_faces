import cv2
import os
import imutils
import math

# Create a cascade classifier
face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

for file in os.listdir('./images'):
    image1 = cv2.imread('./images/' + file)
    image = imutils.resize(image1, 2000)

    result_image = image.copy()

    # Preprocess the image
    grayimg = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # grayimg = cv2.equalizeHist(grayimg)

    # Run the classifiers
    faces = face_cascade.detectMultiScale(grayimg, 1.3, 3, cv2.CASCADE_SCALE_IMAGE, (30, 30))

    if len(faces) != 0:         # If there are faces in the images

        print("Faces detected")
        for f in faces:         # For each face in the image

            # Get the origin co-ordinates and the length and width till where the face extends
            x, y, w, h = [v for v in f]

            # get the rectangle img around all the faces
            cv2.rectangle(image, (x, y), (x + w, y + h), (255, 255, 0), 5)
            sub_face = image[y:y + h, x:x + w]
            # apply a gaussian blur on this new rectangle image
            # sub_face = cv2.GaussianBlur(sub_face, (23, 23), 30)
            # merge this blurry rectangle to our final image
            # result_image[y:y + sub_face.shape[0], x:x + sub_face.shape[1]] = sub_face
            face_file_name = "./result/" + file.split('.')[0] + "/face_" + str(y) + ".jpg"
            cv2.imwrite(face_file_name, sub_face)

    # cv2.imshow("Detected face", result_image)
    cv2.imwrite("./result/" + file, image)
